(set-logic QF_NIA) 
(declare-fun i0 () Int) 
(declare-fun i1 () Int) 
(declare-fun i2 () Int) 
(declare-fun i3 () Int) 
(assert (and (or (and (<= i0 0) (<= i1 0)) (and (<= i0 (+ 0 i1)) (> i1 0))) (and (or (and (<= i0 0) (<= (- i1 1) 0)) (and (<= i0 (+ 0 (- i1 1))) (> (- i1 1) 0))) (and (or (and (<= i2 0) (<= i3 0)) (and (<= i2 (+ 0 i3)) (> i3 0))) (and (or (and (<= i2 1) (<= (- i3 1) 0)) (and (<= i2 (+ 1 (- i3 1))) (> (- i3 1) 0))) (and (< i0 i1) (and (< i2 i3) (and (< i3 i1) (and (>= i1 1) (and (>= (- i1 1) 1) (and (>= (- i2 0) 1) (and (>= (- i2 1) 1) (and (> i0 0) (and (> i0 0) (and (> i0 (+ 0 (- (- i1 1) 1))) (and (> i1 0) (and (> i1 0) (and (> i2 0) (and (> i3 0) (> (- (- i1 1) 1) 0))))))))))))))))))))
(check-sat)
(get-value (i0 i1 i2 i3))
(exit)
