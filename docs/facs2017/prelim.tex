\section{Preliminaries}
\label{sec.prelim}

Notation on terms, term algebras, and equational theories is used as
in~\cite{baader-rewandallthat-1999,goguen-ordsortalg1-1992}.  An {\em
  order-sorted signature} $\Sigma$ is a tuple $\Sigma {=}(S,\leq,F)$
with a finite poset of sorts $(S,\leq)$ and set of function symbols
$F$ typed with sorts in $S$.  The binary relation $\equiv_\leq$
denotes the equivalence relation $(\leq\cup\geq)^+$ generated by
$\leq$ on $S$ and its point-wise extension to strings in $S^*$.  The
function symbols in $F$ can be subsort-overloaded.  To avoid ambiguous
parses they are required to satisfy the condition that, for $w,w' \in
S^*$ and $s,s'\in S$, if $\func{f}{w}{s}$ and $\func{f}{w'}{s'}$ are
in $F$, then $w \equiv_\leq w'$ implies $s \equiv_\leq s'$.  For any
sort $s\in S$, the expression $\tops{s}$ denotes the connected
component of $s$, that is, $\tops{s} = \tops{s}_{\equiv_\leq}$.  A
{\em top sort} in $\Sigma$ is a sort $s \in S$ such that for all $s'
\in [s]$, $s' \leq s$.

Let $X = \{X_s\}_{s\in S}$ denote an $S$-indexed family of disjoint
variable sets with each $X_s$ countably infinite. The {\em set of
  terms of sort $s$} and the {\em set of ground terms of sort $s$} are
denoted, respectively, by $T_\Sigma(X)_s$ and $T_{\Sigma,s}$;
similarly, $T_\Sigma(X)$ and $T_\Sigma$ denote, respectively, the set
of terms and the set of ground terms. $\tcal_\Sigma(X)$ and
$\tcal_\Sigma$ denote the corresponding order-sorted $\Sigma$-term
algebras.  All order-sorted signatures are assumed {\em
  preregular}~\cite{goguen-ordsortalg1-1992}, i.e., each $\Sigma$-term
$t$ has a unique {\em least sort} $\ls{t} \in S$ s.t. $t \in
T_\Sigma(X)_{\ls{t}}$. It is also assumed that $\Sigma$ has {\em
  nonempty sorts}, i.e., $T_{\Sigma,s}\neq \emptyset$ for each $s\in
S$.  The {\em set of variables} of $t$ is written $\vars{t}$ and for a
list of terms $t_1,\ldots,t_n$, $\vars{t_1,\ldots,t_n} = \vars{t_1}
\cup \cdots \cup \vars{t_n}$.  For a term $t\in T_\Sigma(X)$ and
$\olist{x}$ a list of variables in $X$, the expression $t(\olist{x})$
denotes the term $t$ and the fact that each variable in $\vars{t}$
occurs in the list $\olist{x}$. A term $t$ is called \emph{linear} if
and only if each $x \in \vars{t}$ occurs only once in $t$.  For
$S'\subseteq S$, a term is called {\em $S'$-linear} if and only if
each $x \in \vars{t}$ with sort in $S'$ occurs only once in $t$.

A {\em substitution} is an $S$-indexed mapping
$\func{\theta}{X}{T_\Sigma(X)}$ that is different from the identity
only for a finite subset of $X$ and such that $\theta(x) \in
T_\Sigma(X)_s$ if $x \in X_s$, for any $x \in X$ and $s\in S$.  The
identity substitution is denoted by $\ids$ and $\theta|_Y$ denotes the
restriction of $\theta$ to a family of variables $Y\subseteq X$.  The
\emph{domain} of $\theta$, denoted $\dom{\theta}$, is the subfamily of
$X$ such that $x \in \dom{\theta}$ if and only if $\theta(x)\neq x$,
for each $x \in X$.  The \emph{range} of $\theta$ is the set
$\ran{\theta} = \bigcup \{ \vars{\theta(x)} \mid x \in
\dom{\theta}\}$. Substitutions extend homomorphically to terms in the
natural way.  A substitution $\theta$ is called {\em ground} if and
only if $\ran{\theta} = \emptyset$.  The application of a substitution
$\theta$ to a term $t$ is denoted by $\theta t$ and the composition
(in diagrammatic order) of two substitutions $\theta_1$ and $\theta_2$
is denoted by $\theta_1\theta_2$, so that $ \theta_1\theta_2 t$
denotes $\theta_1(\theta_2 t)$.
%% A {\em context} $C$ is a $\lambda$-term of the form $C = \lambda
%% x_1,\ldots,x_n.c$ with $c \in T_\Sigma(X)$ and
%% $\{x_1,\ldots,x_n\}\subseteq\vars{c}$; it can be viewed as an
%% $n$-ary function $(t_1,\ldots,t_n) \mapsto C(t_1,\ldots,t_n) =
%% c\theta$, where $\theta(x_i) = t_i$ for $1 \leq i \leq n$ and
%% $\theta(x) = x$ for $x \notin \{x_1,\ldots,x_n\}$.

A {\em $\Sigma$-equation} is an unoriented pair $t \EQ u$ with $t \in
T_\Sigma(X)_{s}$, $u \in T_\Sigma(X)_{s'}$, and $s \equiv_\leq s'$.  A
{\em conditional $\Sigma$-equation} is a triple $(t,u,\cond)$, denoted
$\ceq{t}{u}{\cond}$, with $t = u$ a $\Sigma$-equation and $\cond$ a
finite conjunction of $\Sigma$-equations; $\Sigma$-equations and
conditional $\Sigma$-equations will be just called $\Sigma$-equations
for brevity.  An {\em equational theory} is a tuple $(\Sigma,E)$, with
$\Sigma$ an order-sorted signature and $E$ a finite collection of
(possibly conditional) $\Sigma$-equations.  An equational theory
$\ecal = (\Sigma,E)$ induces the congruence relation $=_\ecal$ on
$T_\Sigma(X)$ defined for $t,u \in T_\Sigma(X)$ by $t =_\ecal u$ if
and only if $\ecal \ded \eq{t}{u}$, where $\ecal \ded \eq{t}{u}$
denotes $\ecal$-provability by the deduction rules for order-sorted
equational logic in~\cite{meseguer-membership-1998}. For the purpose
of this paper, such inference rules, which are analogous to those of
many-sorted equational logic, are even simpler thanks to the
assumption that $\Sigma$ has nonempty sorts, which makes unnecessary
the explicit treatment of universal quantifiers.  Similarly,
$=_\ecal^1$ denotes provable $\ecal$-equality in {\em one step} of
deduction.
%% The {\em $\ecal$-subsumption} ordering $\sord{\ecal}$ is the binary
%% relation on $T_\Sigma(X)$ defined for any $t,u\in T_\Sigma(X)$ by
%% $t \sord{\ecal} u$ if and only if there is a substitution
%% $\func{\theta}{X}{T_\Sigma(X)}$ such that $t =_\ecal u\theta$.
The expressions $\tcal_{\ecal}(X)$ and $\tcal_\ecal$ (also written
$\tcal_{\Sigma/E}(X)$ and $\tcal_{\Sigma/E}$) denote the quotient
algebras induced by $=_\ecal$ on the term algebras $\tcal_\Sigma(X)$
and $\tcal_\Sigma$, respectively.  $\tcal_{\Sigma/E}$ is called the
{\em initial algebra} of $(\Sigma,E)$. A set of equations $E$ is
called \emph{regular} (resp., {\em linear}) if and only if for any
equation $(\ceq{t}{u}{\cond}) \in E$ $\vars{t} = \vars{u}$ (resp.,
both $t$ and $u$ are linear terms). Moreover, $E$ is called {\em
  collapse-free} for a subset of sorts $S'\subseteq S$ if and only if
for any $t=u \in E$ and for any substitution
$\func{\theta}{X}{T_\Sigma(X)}$ neither $\theta t$ nor $\theta u$ map
to a variable having some sort $s \in S'$.
%% A theory inclusion $(\Sigma,E)\subseteq(\Sigma',E')$,
%% with $\Sigma\subseteq\Sigma'$ and $E\subseteq E'$, is called {\em
%%   protecting} if and only if the unique $\Sigma$-homomorphism
%% $\tcal_{\Sigma/E} \longrightarrow \tcal_{\Sigma'/E'}|_{\Sigma}$ to the
%% \emph{$\Sigma$-reduct}\footnote{For $\Sigma \subseteq \Sigma'$ a
%%   subsignature inclusion and $\mathcal{A}$ a $\Sigma'$-algebra, its
%%   $\Sigma$-\emph{reduct} $\mathcal{A}|_{\Sigma}$ is the
%%   $\Sigma$-algebra obtained from $\mathcal{A}$ by ignoring all
%%   operators and sorts in $\Sigma'\setminus \Sigma$.}  \emph{of the
%%   initial algebra $\tcal_{\Sigma'/E'}$} is a $\Sigma$-isomorphism,
%% written $\tcal_{\Sigma/E} \simeq \tcal_{\Sigma'/E'}|_{\Sigma}$.
%% Intuitively, if a theory inclusion $(\Sigma,E)\subseteq(\Sigma',E')$
%% is protecting, this means that: (i) $=_{E'}$ does not identify terms
%% in $T_\Sigma(X)$ that cannot be proved equal by $=_E$, that is,
%% \emph{no confusion} is added when extending $(\Sigma,E)$ to
%% $(\Sigma',E')$, and (ii) for each sort $s$ of $\Sigma$ and $t' \in
%% T_{\Sigma',s}$ there is a $t \in T_{\Sigma,s}$ such that $t' =_{E'}t$,
%% that is, \emph{no junk} is added to the sorts of $\Sigma$ when
%% extending $(\Sigma,E)$ to $(\Sigma',E')$.

A {\em $\Sigma$-rewrite rule} is a triple $\ccrl{l}{r}{\phi}$, with
$l,r \in T_\Sigma(X)_s$, for some sort $s \in S$, and $\phi =
\bigwedge_{i\in I} t_i = u_i$ a finite conjunction of
$\Sigma$-equations.  A {\em rewrite theory} is a tuple $\rcal =
(\Sigma,E,R)$ with $(\Sigma,E)$ an order-sorted equational theory and
$R$ a finite set of $\Sigma$-rules.  $\rcal = (\Sigma,E,R)$ is called
a {\em topmost rewrite theory} if it has a top sort $\states$ such
that no operator in $\Sigma$ has $\states$ as argument sort and each
rule $\ccrl{l}{r}{\phi}\in R$ satisfies $l,r \in T_\Sigma(X)_\states$
and $l \notin X$.  A rewrite theory $\rcal$ induces a rewrite relation
$\rews_{\rcal}$ on $T_{\Sigma}(X)$ defined for every $t,u \in
T_\Sigma(X)$ by $t \rews_\rcal u$ if and only if there is a rule
$(\ccrl{l}{r}{\phi}) \in R$ and a substitution
$\func{\theta}{X}{T_\Sigma(X)}$ satisfying $t =_E \theta l$, $u =_E
\theta r$, and $E \vdash \theta \phi$. The tuple $\tcal_\rcal =
(\tcal_{\Sigma/E},\rews_\rcal^*)$ is called the {\em initial
  reachability model of $\rcal$}~\cite{bruni-semgenrewths-2006}.

Appropriate requirements are needed to make an equational theory
$\ecal$ {\em admissible}, i.e., {\em executable} in rewriting
languages such as Maude~\cite{clavel-maudebook-2007}.  In this paper,
it is assumed that the equations of $\ecal$ can be decomposed into a
disjoint union $E \uplus B$, with $B$ a collection of regular and
linear structural axioms (such as associativity, and/or commutativity,
and/or identity) for which there exists a {\em matching algorithm
  modulo $B$} producing a finite number of $B$-matching solutions, or
failing otherwise. Furthermore, it is assumed that the equations $E$
can be oriented into a set of (possibly conditional)
{sort-decreasing}, {operationally terminating}, {confluent} rewrite
rules $\overrightarrow{E}$ modulo $B$. The rewrite system
$\overrightarrow{E}$ is {\em sort decreasing} modulo $B$ if and only
if for each $(\crl{t}{u}{\cond}) \in \overrightarrow{E}$ and
substitution $\theta$, $\ls{\theta t} \geq \ls{\theta u}$ if
$(\Sigma,B,\overrightarrow{E}) \ded \theta\cond$.  The system
$\overrightarrow{E}$ is {\em operationally terminating} modulo
$B$~\cite{duran-operterm-2008} if and only if there is no infinite
well-formed proof tree in $(\Sigma,B,\overrightarrow{E})$.
Furthermore, $\overrightarrow{E}$ is {\em confluent} modulo $B$ if and
only if for all $t,t_1,t_2 \in T_\Sigma(X)$, if $t \rews^*_{E/B} t_1$
and $t \rews^*_{E/B} t_2$, then there is $u \in T_\Sigma(X)$ such that
$t_1 \rews^*_{E/B} u$ and $t_2 \rews^*_{E/B} u$.  The term
$\can{t}{E/B} \in T_\Sigma(X)$ denotes the {\em $E$-canonical form} of
$t$ modulo $B$ so that $t \rews_{E/B}^* \can{t}{E/B}$ and $\can{t}{E /
  B}$ cannot be further reduced by $\rews_{E/B}$. Under
sort-decreasingness, operational termination, and confluence, the term
$\can{t}{E/B}$ is unique up to $B$-equality.  For a rewrite theory
$\rcal$, the rewrite relation $\rews_\rcal$ is undecidable in general,
even if its underlying equational theory is admissible, unless
conditions such as coherence~\cite{viry-coherence-2002} are given
(i.e, whenever rewriting with $\rews_{R/E \cup B}$ can be decomposed
into rewriting with $\rews_{E/B}$ and $\rews_{R/B}$).  A rewrite
theory is called \emph{ground convergent} iff it is ground
sort-decreasing, operationally terminating, and ground confluent.
