% !TEX root = main.tex

\section{Concluding Remarks}
\label{sec.concl}


This paper presented \textit{guarded terms}, a technique with the
potential to reduce the symbolic state space and the complexity of
constraints in the rewriting modulo SMT approach. Rewriting modulo
SMT~\cite{rocha-rewsmtjlamp-2017} is a novel symbolic technique to
model and analyze reachability properties of infinite-state
systems. This is a technique in the realm of rewriting logic that can
greatly improve the specification and verification of reachability
properties of open systems such as real-time and cyber-physical
systems. Guarded terms generalize the constrained terms of rewriting
modulo SMT by allowing a term in a symbolic state to have constrained
subterms: these subterms can be seen as a choice operator that is part
of the term structure. They can be composed in parallel and be nested,
thus enabling the succinct encoding of several constrained terms into
one guarded term. Symbolic guarded rewriting has been proved sound for
admissible guarded rules (Theorem~\ref{thm:sim}) and complete for
strongly admissible rules (Theorem~\ref{thm:bisim}) relative to
initial reachability semantics. Soundness ultimately means that
counterexamples to a reachability query unveiled by symbolic guarded
rewriting are actual counterexamples in the initial reachability model
associated to such a theory. On the other hand, completeness can be
used to prove the nonexistence of safety violations using symbolic
guarded rewriting by failing to find counterexamples to a reachability
query.  The potential of guarded terms for reducing the symbolic
state-space, and the complexity and size of constraints has been
illustrated by a running example and a case study. The latter is an
improvement of a previously developed case study where guarded terms
enable the analysis of reachability properties of the CASH scheduling
algorithm~\cite{caccamo2000capacity}.

As future work, it can be important to relax some of the conditions in
Theorem~\ref{thm:bisim} to encompass a broader class of
specifications. The plan is also to explore the use of guarded terms
in enhancing other promising symbolic, rewrite- and narrowing-based
approaches such as the one in~\cite{meseguer-variantbasedscp-2018} for
satisfiability in initial algebras. Other significant case studies on
the use of guarded terms should be pursued. In particular, a new
symbolic rewriting logic semantics of
PLEXIL~\cite{rocha-thesis-2012,rocha-rewsmtjlamp-2017} with guarded
terms can enable the automatic verification of a larger spectrum of
reachability goals. Moreover, it is a challenge to explore the
usefulness of the approach in specifying the symbolic rewriting logic
semantics of other real-time, cyber-physical, open systems. Other SMT
techniques, including state subsumption, backwards reachability,
$k$-induction, and interpolants, should certainly be studied for
rewriting modulo SMT. Also, a new notion of \textit{coherence} for
symbolic specifications in rewriting logic has been proposed
in~\cite{meseguer-symcoherence-2018}; tools for checking the adherence
to this condition for rewriting modulo SMT ---with and without guarded
terms--- specifications need to be built. Finally, a theoretical and a
broader experimental exploration should be done to measure the
computational impact that rewriting with guarded terms could have for
rewriting modulo SMT.
