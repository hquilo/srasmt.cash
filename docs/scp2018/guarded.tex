% !TEX root = main.tex

\section{Guarded Terms}
\label{sec.guarded}

Guarded terms generalize constrained terms with heterogeneous patterns
and nested structures for subterms.  Guarded terms can succinctly
represent various terms by choices of subterms that are guarded by a
constraint.  These subterms represent possible realizations, namely,
those instances in which the constraints are true.


\subsection{Syntax}

Consider the constrained terms
$\pair{t_1}{\phi_1},\pair{t_2}{\phi_2},\ldots,\pair{t_n}{\phi_n}$ with the terms
$t_1,t_2,\ldots,t_n$ of the same sort.  First, a guarded term can be built
by combining these constrained terms in parallel as
\[
\pair{t_1}{\phi_1} \lor \cdots \lor \pair{t_n}{\phi_n},
\]
 semantically
representing the union of the corresponding sets
$\den{t_1}{\phi_1},\ldots,\den{t_n}{\phi_n}$ of ground terms. 
%
 Second, guarded terms
can be \emph{nested} so that the terms $t_i$ may include guarded terms
as subterms. For example, if $f$ and $g$ are unary and
binary function symbols, respectively, then the term 
\[
f(\pair{t_1}{\phi_1} \lor
\pair{f(g(\pair{t_3}{\phi_3}, t_4))}{\phi_2})
\] is a guarded term.  The
guarded subterm $g(\pair{t_3}{\phi_3}, t_4)$ encodes the ground
instances $\theta g(t_3,t_4)$ in which $\theta\phi_3$ is true, for any
ground substitution $\theta$.

In order to avoid confusion between the syntax of constrained terms
and guarded terms, a guarded term 
$\pair{t}{\phi}$ will be written as
$t |_\phi$ in the rest of this paper. 
With this convention, the above-mentioned guarded terms
can be written as $t_1 |_{\phi_1} \lor \cdots \lor t_n |_{\phi_n}$ and
$f(t_1 |_{\phi_1} \lor f(g(t_3 |_{\phi_3}, t_4)) |_{\phi_2})$.
%
%The syntax of guarded terms is formally presented in
%Definition~\ref{def.guarded.guarded}.

\begin{definition}\label{def.guarded.guarded}
Given a signature $\Sigma$, the set of \emph{guarded terms of sort
\sort{s}} is the smallest set $\overline{T}_{\Sigma}(X)_{\sort{s}}$
satisfying the following conditions:%
\footnote{Note that in Maude, kinds  can be considered as top sorts in order-sorted signatures.}

\begin{itemize}
	\item ${T}_{\Sigma}(X)_{\sort{s}} \subseteq \overline{T}_{\Sigma}(X)_{\sort{s}}$;
		
	\item $f(t_1,\ldots,t_n) \in \overline{T}_{\Sigma}(X)_{\sort{s}}$,
    if $f\in \Sigma_{\sort{s}_1\ldots \sort{s}_n, \sort{s}}$ and $t_i
    \in \overline{T}_{\Sigma}(X)_{\sort{s}_i}$ for $1 \leq i \leq n$;
	
	\item $t |_{\phi} \in \overline{T}_{\Sigma}(X)_{\sort{s}}$, if $t
    \in \overline{T}_{\Sigma}(X)_{\sort{s}}$ and $\phi$ is a
    constraint; and
	
	\item $t _1 \lor t_2 \in \overline{T}_{\Sigma}(X)_{\sort{s}}$ for
    guarded terms $t_1, t_2 \in \overline{T}_{\Sigma}(X)_{\sort{s}}$
    of sort~$\sort{s}$.
\end{itemize}
\end{definition}

A signature $G(\Sigma)$ for guarded terms can be built by adding
new sorts and function symbols to the underlying signature $\Sigma$ as
follows:


\begin{itemize}
	\item a new sort $g(\sort{s})$ with a subsort relation $\sort{s} <
    g(\sort{s})$ for each sort \sort{s}; 
    
    \item a subsort relation $g(\sort{s}_1) < g(\sort{s}_2)$
    whenever $s_1$ is a subsort of $s_2$;
	
	\item an operator $\func{f}{g(\sort{s}_1) \cdots
    g(\sort{s}_m)}{g(\sort{s})}$ for each %operator
    $\func{f}{\sort{s}_1 \cdots \sort{s}_m}{\sort{s}}$;

	\item a guard operator $\func{\_|\_}{g(\sort{s}) \times
    \sort{Bool}}{g(\sort{s})}$ for each sort \sort{s}; and

	\item an union operator $\func{\_\!\!\vee\!\!\_}{g(\sort{s}) \times
    g(\sort{s})}{g(\sort{s})}$ for each sort \sort{s},

\end{itemize}

\noindent
where constraints have sort \sort{Bool} (in the built-in subsignature
$\Sigma_0 \subseteq \Sigma$).
%
By construction, a guarded term $u \in
\overline{T}_{\Sigma}(X)_{\sort{s}}$ of sort \sort{s} is a
$G(\Sigma)$-term of sort $g(\sort{s})$.


\begin{lemma}
\label{lemma:exsig}
Given an order-sorted signature $\Sigma$, a guarded term $u$ of sort
\sort{s} in $\overline{T}_{\Sigma}(X)$ is a term of sort $g(\sort{s})$
in $T_{G(\Sigma)}(X)$, and vice versa.
\end{lemma}


\begin{proof}
By induction on the structure of $u$.
%
\begin{itemize}
	\item If $u \in {T}_{\Sigma}(X)_{\sort{s}}$,  $u \in {T}_{G(\Sigma)}(X)_{\sort{g(s)}}$ because $\sort{s} < \sort{g(s)}$ by construction.
%
	\item If $u$ is $f(u_1,\ldots,u_n)$,
	 $u_i \in {T}_{G(\Sigma)}(X)_{g(\sort{s}_i)}$ for $1 \leq i \leq n$
	 by induction hypothesis,
	 and
	$u \in {T}_{G(\Sigma)}(X)_{\sort{g(s)}}$, since $f : g(\sort{s}_1) \ldots g(\sort{s}_n) \to g(\sort{s}) \in G(\Sigma)$.
%
	\item If $u$ is $v |_{\phi}$, then $v \in
    {T}_{G(\Sigma)}(X)_{\sort{g(s)}}$ by induction hypothesis.  It
    follows that $u \in {T}_{G(\Sigma)}(X)_{\sort{g(s)}}$ because
    $\_|\_ : g(\sort{s}) \times \sort{Bool} \to g(\sort{s}) \in
    G(\Sigma)$.
%
	\item If $u$ is $u_1 \vee u_2$, $u_1, u_2 \in
    {T}_{G(\Sigma)}(X)_{\sort{g(s)}}$ for $1 \leq i \leq n$ by
    induction hypothesis.  It follows that $u \in
    {T}_{G(\Sigma)}(X)_{\sort{g(s)}}$, since $\_\!\!\vee\!\!\_ :
    g(\sort{s}) \times g(\sort{s}) \to g(\sort{s}) \in G(\Sigma)$.
%
\end{itemize}
Hence, $u \in \overline{T}_{\Sigma}(X)_{\sort{s}}$ implies $u \in {T}_{G(\Sigma)}(X)_{\sort{g(s)}}$.
%
The other direction is similar. 
\end{proof}



\begin{example}\label{ex:syntax}
In the example in Section~\ref{sec.rewsmt}, a term $I + J
+ 1$ is added to the right channel by the first rule if the
condition $I + J \meq{3} 0$ holds. A
one-step symbolic rewrite step involves two cases:
either $I + J \meq{3} 0$ holds or
not. Thus, $n$-rewrite steps with this rule can yield $2^n$ different cases.
%
For example, given the left channel $A\;B\;C$, there are $8$ different
cases:
%
\begin{align*}
&
\mathit{none}
&&
(A + B + 1)\;(B + C + 1)
\\
&
(A + B + 1)
&&
(A + B + 1)\;(C + A + 1)
\\
&
(B + C + 1)
&&
(B + C + 1)\;(C + A + 1)
\\
&
(C + A + 1)
&&
(A + B + 1)\;(B + C + 1)\;(C + A + 1).
\end{align*}

A set of terms can be succinctly encoded as a guarded term.
Consider guarded terms of the form $\mathit{elm}(I,J)$ defined as
\[
\mathit{elm}(I,J) \;=\; I + J + 1 |\usub{I + J \meq{3} 0}
\;\vee\; \mathit{none} |\usub{I + J \nmeq{3} 0}.  
\]
Then, the
guarded term
$\mathit{elm}(I_1,J_1)\ \mathit{elm}(I_2,J_2)\ \cdots\ \mathit{elm}(I_N,
J_N)$ can encode the $2^n$ different states
with respect to the satisfaction of the $n$
conditions $I_i + J_i \meq{3} 0$, for
$1 \leq i \leq N$.
%
For example, the above right-hand channels reachable from
$A\;B\;C\parallel \mathit{none}$ can be encoded as
$\mathit{elm}(A,B)\;\mathit{elm}(B,C)\;\mathit{elm}(C,A)$.
\end{example}


\subsection{Semantics}

A guarded term $u$ represents a (possibly infinite) set of ground
terms that is denoted by $\llbracket u \rrbracket$.  Intuitively, $\llbracket
u |_\phi \rrbracket$ is the set of all ground instances of $u$ that
satisfy the constraint $\phi$, and $\llbracket u_1 \vee u_2 \rrbracket$ is the union
of the sets $\llbracket u_1 \rrbracket \cup \llbracket u_2
\rrbracket$.
%
%To formally define the semantics of guarded terms, \emph{ground}
%guarded terms are first considered. 
 The ground semantics in
Definition~\ref{def:ground-semantics} is straightforward, because the
constraints in ground guarded terms can be determined as either
$\mathit{true}$ or $\mathit{false}$ in the underlying built-in algebra
$\model$.  Specifically, a ground guarded term $u |_\phi$ represents
either $\llbracket u \rrbracket$ if $\phi$ is satisfiable in $\model$
or the empty set if $\phi$ is not satisfiable in $\model$.

\begin{definition}\label{def:ground-semantics}
Given a ground guarded term $u \in \overline{T}_{\Sigma}$,
the set $\llbracket u \rrbracket \subseteq T_\Sigma$ of  ground terms represented by $u$ 
is inductively defined as follows:
\[
\begin{aligned}
\llbracket t \rrbracket &= \{t' \in T_\Sigma \mid t' =_E t\}\;\;\mbox{if $t \in {T}_{\Sigma}$},
\\
\llbracket f(u_1,\ldots,u_n) \rrbracket &=  \{t \in \llbracket f(t_1,\ldots,t_n)\rrbracket \mid  t_i \in \llbracket u_i\rrbracket, 1 \leq i \leq n  \},
\\
\llbracket u |_\phi \rrbracket &=
    \begin{cases}
    \llbracket u \rrbracket &\mbox{if}\ \model \models \phi 
    \\
    \emptyset &\mbox{if}\ \model \not \models \phi,
    \end{cases}
\\
\llbracket u_1  \vee u_2  \rrbracket &= \llbracket u_1 \rrbracket \cup \llbracket u_2 \rrbracket.
\end{aligned}
\]
\end{definition}


\begin{example}\label{ex:semantics-ground}
Consider the ground guarded term in Example~\ref{ex:syntax}:
\[
u \;=\; (3 + 1 + 1 |_{3 + 1\meq{3} 0} \vee  \mathit{none} |_{3 + 1 \nmeq{3} 0})\ (5 + 4 + 1 |_{5 + 4\meq{3} 0} \vee  \mathit{none} |_{5 + 4 \nmeq{3} 0})
\]
It represents the set $\llbracket u \rrbracket = \{t' \mid t' =_E
 \mathit{none}\ (5 + 4 + 1)\}$, because $ 3 + 1
\nmeq{3} 0$ and $5 + 4
\meq{3} 0$.  Note that $5 + 4 + 1 \in
\llbracket u \rrbracket$, since $5 + 4 + 1 =_E  \mathit{none}\ (5 + 4 +
1)$.
\end{example}

The semantics of guarded terms in general, introduced by
Definition~\ref{def:semantics}, is based on the semantics of ground
guarded terms.  Each ground instance $\theta u$ of a guarded term $u$
under a ground substitution $\theta$ defines the set $\llbracket
\theta u \rrbracket$ of ground terms.  The set $\llbracket u
\rrbracket$ is then defined as the union of all the sets given by the ground instances
of the guarded term $u$.

\begin{definition}\label{def:semantics}
Given a guarded term $u \in \overline{T}_{\Sigma}(X)$, the set of all
ground terms represented by $u$ is defined as 
$\llbracket u \rrbracket
= \{t \in T_\Sigma \mid (\exists \func{\theta}{X}{T_\Sigma})\; t \in
\llbracket \theta u \rrbracket \}.$
\end{definition}


It is worth noting that the semantics of non-ground guarded terms
cannot be defined in the same way as the case of ground guarded terms.
Specifically, the second condition  in
Definition~\ref{def:ground-semantics} for ground guarded terms:
\[\llbracket f(u_1,\ldots,u_n) \rrbracket = \{t \in
\llbracket f(t_1,\ldots,t_n)\rrbracket \mid t_i \in \llbracket
u_i\rrbracket, 1 \leq i \leq n \}\]
 is
generally \emph{not} applicable to non-ground guarded terms.
For example, 
consider an unsorted signature $\Sigma$ with two
different constants $c$ and $d$.  
%
Notice that $\llbracket f(x |_{x \neq y}, y |_{x = y})\rrbracket =
\emptyset$, but $\llbracket x |_{x \neq y} \rrbracket = \{c,d\}$ and
$\llbracket y |_{x = y} \rrbracket = \{c, d\}$.


\begin{example}\label{ex:semantics}
Consider the nested guarded term in Example~\ref{ex:syntax}:
\[(I_1 + J_1 + 1 |_{I_1 + J_1\meq{3} 0} \vee  \mathit{none} |_{I_1 + J_1 \nmeq{3} 0})\ (I_2 + J_2 + 1 |_{I_2 + J_2\meq{3} 0} \vee  \mathit{none} |_{I_2 + J_2 \nmeq{3} 0})\]
Under the substitution $\{I_i \mapsto x_i, J_i \mapsto y_i \mid i = 1,
2\}$, the guarded term represents the set of following terms:
%
\begin{align*}
\llbracket (x_1 + y_1 +1)\ (x_2 + y_2 + 1) \rrbracket &\qquad\mbox{if}\;\; x_1 + y_1\meq{3} 0 \;\wedge\; x_2 + y_2\meq{3} 0,\\
\llbracket  \mathit{none}\ (x_2 + y_2 + 1) \rrbracket &\qquad\mbox{if}\;\; x_1 + y_1 \nmeq{3} 0 \;\wedge\; x_2 + y_2\meq{3} 0,\\
\llbracket (x_1 + y_1 +1)\  \mathit{none} \rrbracket &\qquad\mbox{if}\;\; x_1 + y_1\meq{3} 0 \;\wedge\; x_2 + y_2 \nmeq{3} 0,\\
\llbracket  \mathit{none}\  \mathit{none} \rrbracket &\qquad\mbox{if}\;\; x_1 + y_1 \nmeq{3} 0 \;\wedge\; x_2 + y_2 \nmeq{3} 0.
\end{align*}
%
Each one of these cases includes an infinite number of ground terms
because there are an infinite number of ground substitutions that
satisfy each guard. It can be seen that the number of cases increases
exponentially: if the number of elements is $n$, the number of
different cases is $2^n$.
\end{example}


The notion of constrained terms in Section~\ref{sec.rewsmt} can be
seen as a special case of guarded terms, because the denotation
$\dens{t}{\phi}$ of a constrained term $\pair{t}{\phi}$ coincides with the semantics of
a guarded term $t |_\phi$.

\begin{lemma}
Given a $\Sigma$-term $t \in T_\Sigma(X)$ and a constraint $\phi$,
$\llbracket t \rrbracket_\phi = \llbracket t |_\phi \rrbracket$ holds.
\end{lemma}

\begin{proof}
\begin{inparaenum}[(1)]
\item Suppose that $t' \in \llbracket t \rrbracket_\phi$.  By
  \cite[Definition 6]{rocha-rewsmtjlamp-2017}, there exists a ground
  substitution $\theta: X \to T_{\Sigma}$ such that $t' =_{E} \theta
  t$ and $\model \models \theta\phi$ hold.  By
  Def.~\ref{def:ground-semantics}, $t' \in \llbracket \theta t
  |_{\theta\phi} \rrbracket$, and by Def.~\ref{def:semantics}, follows
  $t' \in \llbracket t |_\phi \rrbracket$.  Therefore, $\llbracket t
  \rrbracket_\phi \subseteq \llbracket t |_\phi \rrbracket$.
%
\item Suppose that $t' \in \llbracket t |_\phi \rrbracket$.
By Def.~\ref{def:semantics}, 
there exists a ground substitution $\theta: X \to T_{\Sigma}$
such that 
$t' \in \llbracket \theta t |_{\theta \phi} \rrbracket$. 
By Def.~\ref{def:ground-semantics}, $\model \models \theta\phi$,
and $t' \in \llbracket \theta t  \rrbracket$, i.e., $t' =_E \theta t$ because $t$ is a $\Sigma$-term. 
Therefore, $t' \in \llbracket t \rrbracket_\phi$.
\end{inparaenum}
\end{proof}



\subsection{Equivalence}

Guarded terms $u_1$ and $u_2$ are called \emph{equivalent}, written
$u_1 \equiv u_2$, when they represent the same set of ground terms, that
is, $\llbracket u_1 \rrbracket = \llbracket u_2 \rrbracket$.  For
example, guarded terms that are identical up to variable renaming
are clearly equivalent, since they have the same set of ground instances.  Guarded
terms $u_1$ and $u_2$ are called \emph{ground equivalent}, written $u_1 \equiv_g u_2$,
when their ground instances by the same substitution are equivalent.

\begin{definition}
  Let $u_1, u_2 \in \overline{T}_{\Sigma}(X)$:
  \begin{enumerate}
  \item $u_1 \equiv u_2$ iff $\llbracket u_1 \rrbracket = \llbracket u_2 \rrbracket$.
  \item $u_1 \equiv_g u_2$ iff $\llbracket \theta u_1 \rrbracket =
    \llbracket \theta u_2 \rrbracket$, for every ground substitution
    $\func{\theta}{X}{T_\Sigma}$.
  \end{enumerate}
\end{definition}


The ground equivalence $u_1 \equiv_g u_2$ implies 
the equivalence $u_1 \equiv u_2$ for guarded terms $u_1$ and $u_2$ by definition,
but the converse may not hold.
For example,
$0 |_{x = 0} \equiv 0|_{x
  = 1}$, because $\llbracket 0 |_{x = 0} \rrbracket = \llbracket 0
|_{x = 1} \rrbracket = \{0\}$.  But $0 |_{x = 0} \not\equiv_g 0_{x =
  1}$, because $\llbracket \theta(0 |_{x = 0}) \rrbracket = \{0\}$ and
$\llbracket \theta(0 |_{x = 1}) \rrbracket = \emptyset$ for substitution $\theta =
\{x \mapsto 0\}$, provided that $0 \neq 1$. 





The ground equivalence $\equiv_g$ between guarded terms  is a \emph{congruence} as shown in
Lemma~\ref{lemma:congruence} below. On the contrary, the equivalence
$\equiv$ violates the congruence rules.  Specifically, 
a guarded term $u$ and its
variable renaming $\sigma u$ always satisfy $u \equiv \sigma u$, but $u
|_\phi$ and $(\sigma u) |_\phi$ may not be equivalent.
%
For example, $0 |_{x = 0}
\equiv 0_{y = 0}$ but $(0 |_{x = 0}) |_{x = 1} \not\equiv (0 |_{y =
  0}) |_{x = 1}$, since $\llbracket (0 |_{x = 0}) |_{x = 1}
\rrbracket = \emptyset$ and $\llbracket (0 |_{y = 0}) |_{x = 1}
\rrbracket = \{0\}$, provided that $0$ and $1$ are different.

\begin{lemma}[Congruence of Ground Equivalence]\label{lemma:congruence}
Given two guarded terms $u,v \in \overline{T}_{\Sigma}(X)$, if $u \equiv_g v$, then:
\begin{itemize}
	\item $\sigma u \,\equiv_g\, \sigma v$ for a substitution $\func{\sigma}{X}{T_\Sigma(X)}$.		
	\item $f(u_1, \ldots, u, \ldots, u_n) \,\equiv_g\, f(u_1, \ldots, v, \ldots, u_n)$.
	\item $u |_\phi \,\equiv_g\, v |_\phi$.
	\item $u_1 \vee \cdots \vee u \vee \cdots \vee u_n \,\equiv_g\, u_1 \vee \cdots \vee v \vee \cdots \vee u_n$.
\end{itemize}
\end{lemma}


\begin{proof}
%
Let $\theta : X \to T_\Sigma$ be any ground substitution. 
$\llbracket \theta u \rrbracket = \llbracket \theta v\rrbracket$ by assumption.
\begin{inparaenum}[(1)]
	\item Since $\theta \sigma$ is a ground substitution, 
		 $\llbracket \theta (\sigma u) \rrbracket = \llbracket (\theta \sigma) u \rrbracket = \llbracket (\theta \sigma) v \rrbracket = \llbracket \theta (\sigma v) \rrbracket$.
	\item 
	$\llbracket  \theta f( u_1, \ldots, u, \ldots, u_n) \rrbracket$ is by Def.~\ref{def:ground-semantics}:
	\begin{align*}
	& \{t' \in \llbracket f(t_1,\ldots, t, \ldots, t_n)\rrbracket \mid  t_1 \in \llbracket \theta u_1\rrbracket, \ldots, t \in \llbracket \theta u\rrbracket, \ldots, t_n \in \llbracket \theta u_n\rrbracket  \}
	\\
	=\;& \{t' \in \llbracket f(t_1,\ldots, t, \ldots,t_n)\rrbracket \mid  t_1 \in \llbracket \theta u_1\rrbracket, \ldots, t \in \llbracket \theta v\rrbracket, \ldots, t_n \in \llbracket \theta u_n\rrbracket  \}
	\end{align*}
	that is $\llbracket  \theta f( u_1, \ldots, v, \ldots, u_n) \rrbracket$. 
	\item If $\model \models \theta\phi$, $\llbracket \theta u |_{\theta\phi}\rrbracket = \llbracket \theta u \rrbracket = \llbracket \theta v \rrbracket = \llbracket \theta v |_{\theta\phi}\rrbracket$,
	and if $\model \not\models \theta\phi$, $\llbracket \theta u |_{\theta\phi}\rrbracket  = \llbracket \theta v |_{\theta\phi}\rrbracket = \emptyset$.
	\item $\llbracket \theta u_1 \vee \cdots \vee \theta u \vee \cdots \vee \theta u_n\rrbracket$ is
	$\llbracket \theta u_1\rrbracket \cup \cdots \cup \llbracket \theta u \rrbracket \cup \cdots \cup \llbracket \theta u_n\rrbracket$,
	which is equal to
	$\llbracket \theta u_1\rrbracket \cup \cdots \cup \llbracket \theta v \rrbracket \cup \cdots \cup \llbracket \theta u_n\rrbracket $,
	which is $\llbracket \theta u_1 \vee \cdots \vee \theta v \vee \cdots \vee \theta u_n\rrbracket$ by definition.
\end{inparaenum}
\end{proof}





The semantics of $\vee$ is associative and commutative with respect to
ground equivalence, because it is defined as a set union operation.
From now on, the expression $u_1 \vee \cdots \vee u_n$ will be written
without parentheses using this fact.


\begin{corollary}[Associativity and Commutativity of $\vee$]
Given guarded terms $u_1, u_2, u_3 \in \overline{T}_{\Sigma}(X)$: 
  \begin{enumerate}
	\item $u_1 \vee u_2 \equiv_g u_2 \vee u_1$.
  	\item $(u_1 \vee u_2) \vee u_3 \equiv_g u_1\vee (u_2 \vee u_3)$.
  \end{enumerate}

\end{corollary}


\begin{proof}
Let $\theta : X \to T_\Sigma$ be a  ground substitution.
$\llbracket \theta (u_1 \vee  u_2)\rrbracket  = \llbracket \theta u_1 \rrbracket \cup \llbracket \theta u_2 \rrbracket$
and 
$\llbracket\theta(u_2 \vee u_1)\rrbracket = \llbracket \theta u_2 \rrbracket \cup \llbracket \theta u_1 \rrbracket$.
Since $\cup$ is commutative,  $\llbracket \theta (u_1 \vee  u_2)\rrbracket = \llbracket\theta(u_2 \vee u_1)\rrbracket$.
%
Similarly,
$\llbracket \theta (u_1 \vee  u_2) \vee u_3 \rrbracket =  \llbracket\theta(u_1 \vee (u_2 \vee u_3))\rrbracket = \llbracket \theta u_1 \rrbracket \cup \llbracket \theta u_2 \rrbracket \cup \llbracket \theta u_3 \rrbracket$.
\end{proof}



Guarded terms with different syntactic structures can be ground equivalent (and
thus they are also equivalent by definition).  Lemma~\ref{lemma:geq}
identifies some properties of ground equivalence between guarded terms.



\begin{lemma}[Ground Equivalence Rules]
\label{lemma:geq}
For %guarded terms 
$u_1, \ldots, u_n \in \overline{T}_{\Sigma}(X)$:

\begin{itemize}
	\item $\big(\bigvee_{i=1}^n u_i \big) |_\phi \,\equiv_g\, \bigvee_{i=1}^n (u_i  |_\phi)$
	
	\item $ f(u_1,\ldots \big(\bigvee_{i=1}^n u_j^i\big) \ldots,u_n) \,\equiv_g\, \bigvee_{i=1}^n f(u_1,\ldots u_j^i \ldots,u_n)$

	\item $\big(u |_\varphi\big) |_\phi \,\equiv_g\, u |_{\varphi \wedge \phi}$
	
	\item $f(u_1,\ldots u_j |_\phi \ldots,u_n) \,\equiv_g\, f(u_1,\ldots u_j \ldots,u_n) |_\phi$ 

\end{itemize}
\end{lemma}

\begin{proof}
%It suffices to show that they hold for any ground substitution $\theta : X \to T_\Sigma$.
Let $\theta : X \to T_\Sigma$ be a ground substitution. 
\begin{inparaenum}[(1)]
	 \item %For a ground substitution~$\theta$, 
	 If $\model \models \theta\phi$, 
	 both $\llbracket \big(\bigvee_{i=1}^n \theta u_i \big) |_{\theta\phi} \rrbracket$ and $\llbracket \bigvee_{i=1}^n (\theta u_i  |_{\theta\phi}) \rrbracket$
	 are $\bigcup_{i=1}^n  \llbracket\theta u_i \rrbracket$.
	 If $\model \not\models \theta\phi$, both are $\emptyset$.
	 %
	\item  %For a ground substitution~$\theta$, 
	$\llbracket f(\theta u_1,\ldots, \big(\bigvee_{i=1}^n \theta u_j^i\big), \ldots,\theta u_n) \rrbracket$
	is by Def.~\ref{def:ground-semantics}:
	\[
	\begin{aligned}
	&\textstyle
	\{t \in \llbracket f(t_1,\ldots,t_n)\rrbracket \mid  t_1 \in \llbracket \theta u_1\rrbracket, \ldots, t_j \in \llbracket \bigvee_{i=1}^n \theta u_j^i\rrbracket, \ldots, t_n \in \llbracket \theta u_n\rrbracket \}
	\\
	=\;&\textstyle
	\{t \in \llbracket f(t_1,\ldots,t_n)\rrbracket \mid  t_1 \in \llbracket \theta u_1\rrbracket, \ldots, t_j \in \bigcup_{i=1}^n  \llbracket \theta u_j^i\rrbracket, \ldots, t_n \in \llbracket \theta u_n\rrbracket \}
	\\
	=\;&\textstyle
	\bigcup_{i=1}^n  \{t \in \llbracket f(t_1,\ldots,t_n)\rrbracket \mid  t_1 \in \llbracket \theta u_1\rrbracket, \ldots, t_j \in  \llbracket \theta u_j^i\rrbracket, \ldots, t_n \in \llbracket \theta u_n\rrbracket \}
	\end{aligned}
	\]
	i.e., $\llbracket \bigvee_{i=1}^n f(\theta u_1,\ldots, \theta u_j^i, \ldots,\theta u_n) \rrbracket$
	by Def.~\ref{def:ground-semantics}.
	%
	\item %For a ground substitution~$\theta$, 
	If $\model \models \theta\phi$ and $\model \models \theta\varphi$,
	 both $\llbracket \big(\theta u |_{\theta\varphi}\big) |_{\theta\phi}\rrbracket$ and $\llbracket \theta u |_{\theta\varphi \wedge \theta\phi} \rrbracket$
	 are $\llbracket \theta u \rrbracket$,
	 and otherwise, both are $\emptyset$. 
	 %
	 \item
	 If $\model \models \theta\phi$,
	 both $\llbracket f(\theta u_1,\ldots \theta u_j |_{\theta\phi} \ldots,\theta u_n)\rrbracket $
	 and $\llbracket f(\theta u_1,\ldots \theta u_j \ldots,\theta u_n) |_{\theta\phi} \rrbracket$
	 are  $\llbracket f(\theta u_1,\ldots \theta u_j \ldots,\theta u_n) \rrbracket$,
	 and if $\model \not\models \theta\phi$, both are $\emptyset$.
	 %
\end{inparaenum}
\end{proof}



Notice that these equivalence rules can be considered as algebraic properties of guarded term constructors 
such as $\bigvee$ and $|$.
%
Guarded terms can be simplified using these equivalence
rules.  For example, 
\begin{align*}
f(u_1 \vee u_2, v_1 \vee v_2) \,\equiv_g\,
f(u_1, v_1 \vee v_2) \vee f(u_2, v_1 \vee v_2) &\,\equiv_g\,
\textstyle
\bigvee_{i,j \in \{1,2\}} f(u_i,v_j),
\\
f(u |_{\phi}, v |_{\psi})
\,\equiv_g\, f(u, v) |_{\phi} |_{\psi} &\,\equiv_g\, f(u, v) |_{\phi
  \wedge \psi}.
\end{align*}

By repeatedly applying the equivalences in Lemma~\ref{lemma:geq},
given a guarded term, a (ground) equivalent guarded term can be
obtained that is in \emph{standard form}, namely, a combination of
normal constrained terms, as shown in Theorem~\ref{thm:standard}.


\begin{theorem}[Standard Form]
\label{thm:standard}
Every $u \in \overline{T}_{\Sigma}(X)$ is ground equivalent to a
guarded term in \emph{standard form} as follows, where $t_1, \ldots, t_n \in T_\Sigma(X)$:
\[
t_1 |_{\phi_1} \lor t_2
|_{\phi_2} \lor \cdots \lor t_n |_{\phi_n}
\]
\end{theorem}

\begin{proof}
By induction on the depth of guarded term $u\in \overline{T}_{\Sigma}(X)$.
If the depth is $0$ (i.e., $u$ is a constant or a variable), $u$ is clearly in the standard form. 
%
Suppose that the theorem holds for all guarded terms whose depth is less than $k$,
and $u$ has depth $k$. There are three cases:

\begin{itemize}	
	\item $u$ is $v |_\phi$. By induction hypothesis, $v \equiv_g
    \bigvee_{j} t_{j} |_{\phi_{j}}$.  By Lemma~\ref{lemma:congruence},
    it follows that $u \equiv_g (\bigvee_{j} t_{j} |_{\phi_{j}})
    |_\phi$.  By Lemma~\ref{lemma:geq}, $(\bigvee_{j} t_{j}
    |_{\phi_{j}}) |_\phi \equiv_g \bigvee_{j} t_{j} |_{\phi_{j} \wedge
      \phi}$, which is in the standard form.
	%
	\item $u$ is $v_1 \vee \cdots \vee v_n$.  By induction hypothesis,
    $v_i \equiv_g \bigvee_{j_i} t_{i,j_i} |_{\phi_{i,j_i}}$.  By
    Lemma~\ref{lemma:congruence}, $u \equiv_g \bigvee_{i=1}^n
    \bigvee_{j_i} t_{i,j_i} |_{\phi_{i,j_i}}$, which is in the
    standard form, because $\vee$ is associative.
	%
	\item $u$ is $f(v_1, \ldots, v_n)$.  By induction hypothesis, $v_i
    \equiv_g \bigvee_{j_i} t_{i,j_i} |_{\phi_{i,j_i}}$.
%
	By Lemma~\ref{lemma:congruence}, $u \equiv_g
  f(\bigvee_{j_1} t_{1,j_1} |_{\phi_{1,j_1}}, \ldots, \bigvee_{j_n}
  t_{n,j_n} |_{\phi_{n,j_n}})$.
	%
	By Lemma~\ref{lemma:geq},
	\[
	\begin{aligned}
	&
	f(\bigvee_{j_1} t_{1,j_1} |_{\phi_{1,j_1}}, \ldots, \bigvee_{j_n} t_{n,j_n} |_{\phi_{n,j_n}})
	\\
	\,\equiv_g\,
	&
	\bigvee_{j_1} \ldots \bigvee_{j_n} f( t_{1,j_1} |_{\phi_{1,j_1}}, \ldots,  t_{n,j_n} |_{\phi_{n,j_n}}) 
	\\
	\,\equiv_g\,
	&
	\bigvee_{j_1,\ldots,j_n} f( t_{1,j_1} , \ldots,  t_{n,j_n} )|_{\bigwedge_{i=1}^n \phi_{i,j_i}},
	\end{aligned}
	\]
	which is in the standard form.
\end{itemize}
Consequently, $u$ is ground equivalent to a guarded term in standard form.
\end{proof}


A guarded term in standard form has a ``flat'' structure in which
parallel combinations $\vee$ and constraints $\phi$ appear only at the
top.  However, a \emph{nested} guarded term can be exponentially
smaller than a ground equivalent one in the standard form.  For
example, a nested guarded term $f(u_1 \vee v_1, \ldots, u_n \vee v_n)$
of size $O(n)$ is equivalent to its standard form
\[
\bigvee_{w_i \in \{u_i,
  v_i\}} f(w_1, \ldots, w_n)
\] of size $O(2^n)$.
%
This explains why guarded terms are very useful for succinctly
representing the symbolic state space. It is also important to point
out that standard forms are not unique; for instance,
$0|_{\textit{true}}$ and $0|_{\textit{true}} \lor
0|_{\textit{false}}$ are both standard forms of $0$.


\begin{example}
Consider the nested guarded term in Example~\ref{ex:semantics}:
\[
(I_1 + J_1 + 1 |_{I_1 + J_1\meq{3} 0} \vee  \mathit{none} |_{I_1 + J_1 \nmeq{3} 0})\ (I_2 + J_2 + 1 |_{I_2 + J_2\meq{3} 0} \vee  \mathit{none} |_{I_2 + J_2 \nmeq{3} 0})
\]
It is equivalent to the following guarded term in the standard form
obtained by repeatedly applying the equivalences in Lemma~\ref{lemma:geq}:
\begin{align*}
&
  (I_1 + J_1 + 1)\ (I_2 + J_2 + 1) |_{I_1 + J_1\meq{3} 0 \;\wedge\; I_2 + J_2\meq{3} 0} 
\\
\vee\;\;&
   \mathit{none}\ (I_2 + J_2 + 1) |_{I_1 + J_1 \nmeq{3} 0 \;\wedge\; I_2 + J_2\meq{3} 0}
\\
\vee\;\;&
   (I_1 + J_1 + 1)\  \mathit{none} |_{I_1 + J_1\meq{3} 0 \;\wedge\; I_2 + J_2 \nmeq{3} 0}  
\\
\vee\;\;&  \mathit{none}\  \mathit{none} |_{I_1 + J_1 \nmeq{3} 0 \;\wedge\; I_2 + J_2 \nmeq{3} 0},
\end{align*}
where each case is expressed as a single disjunct.  As explained in
Example~\ref{ex:semantics}, since the number of cases increases
exponentially, the size of the standard form also increases
exponentially with respect to the size of the original one.
\end{example}
