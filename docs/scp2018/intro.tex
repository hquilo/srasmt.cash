% !TEX root = main.tex

\section{Introduction}
\label{sec.intro}

Open systems are a broad class of concurrent systems used to meet
today's software and hardware demands. They are characterized by their
ongoing exchange with the environment by receiving and issuing
stimuli.  The use of formal methods in the design and analysis of open
systems is well motivated and a current trend of research. This is
mainly because open systems include mission-critical, reactive,
real-time, and cyber-physical applications spanning over several
domains: with inherently internal \textit{and} external
non-determinism, they naturally fall outside the scope of
well-established modeling techniques and tools, which assume, e.g., a
finite and tractable reachable state-space such as algorithmic model
checking.

Rewriting logic~\cite{meseguer-rltcs-1992}, a logic of concurrent
change that can naturally deal with state and with concurrent
computations, offers a clean ---yet highly expressive--- mathematical
foundation to assign formal meaning to open system computation.  In a
rewrite theory, the unit of specification of a concurrent system in
rewriting logic, concurrent computations are axiomatized by (possibly
conditional) rewrite rules of the form $l \to r$, meaning that any
system state satisfying the pattern $l$ is to transition to a system
state satisfying the pattern $r$. For any given state, many rewrite
rules can be active (or even the same rule at different parts of the
state), thus allowing for non-determinism. The crucial observation for
modeling open systems in rewriting logic is that rules can be equipped
with extra variables in the right-hand side, as in $l(\vec{x}) \to
r(\vec{x},\vec{y})$, meaning that the ``data'' captured by the
variables $\vec{y}$ in a system transition is under the control of the
non-deterministic environment.

Computing with ---and, ultimately, verifying--- a rewrite theory
specifying an open system and whose rules have extra-variables in
their right-hand side is another kettle of fish entirely, in
comparison with the more restricted version of a rewrite theory in
which extra-variables are not present. The crux of the matter lies in
choosing how to instantiate the extra-variables $\vec{y}$ when
computing an open transition. Even if the variables $\vec{y}$ range
over a finite domain, which is seldom the case, an external entity in
the form of a strategy is required for simulation and verification
purposes, rendering the formal analysis task at hand more specific
than usually desired. In the more general case, where some of the
variables in $\vec{y}$ range over infinite domains (e.g., integer or
real numbers), the situation is far worse: there is no effective way
of computing the transition relation of the open system. Although it
is perfectly possible to specify an open system with current
frameworks such as rewriting logic, the more general question remains
unanswered: how to effectively simulate and analyze an open system in
rewriting logic ---or any other formal framework of concurrency?

The above question can be approached by appealing to symbolic methods,
under the promise of capturing and taming the many complexities
arising when formally dealing with the simulation and analysis of open
systems. The main idea is that symbolic computation can naturally deal
with the potentially infinitary behavior induced by the external
environment. Rewriting modulo SMT~\cite{rocha-rewsmtjlamp-2017} is a
symbolic specification and verification method for rewriting
logic. The main key feature of rewriting modulo SMT is that it extends
the standard notion of rewrite theory with SMT-based reasoning on
constrained terms, which can be used to specify symbolic system
states.

The rewriting modulo SMT symbolic method seamlessly combines rewriting
modulo equational theories, SMT solving, and model checking.  In
rewriting modulo SMT, states are represented as symbolic constrained
terms $\pair{t}{\phi}$ with $t$ a term with variables ranging over the
\emph{built-ins} (the sorts handled by the SMT solver) and $\phi$ a
SMT-solvable formula. State transitions are symbolic rewrite steps
between constrained terms.  In one rewrite step from
$\pair{t_1}{\phi_1}$ to $\pair{t_2}{\phi_2}$, possibly infinitely many
instances of $t_1$ can be rewritten to instances of $t_2$; namely,
those ground instances of $t_1$ that satisfy the constraint $\phi_1$
result in a ground instance of $t_2$ satisfying the constraint
$\phi_2$. For a rewrite rule with extra-variables $\vec{y}$ in its
right-hand side, this means that the open transition from
$\pair{t_1}{\phi_1}$ axiomatized by such a rule can be computed by
including the data represented by $\vec{y}$ in $t_2$ and adding the
corresponding constraints for these variables in $\phi_2$. This is the
main reason why rewriting modulo SMT is well-suited for symbolically
proving (or disproving) safety properties of open systems.

However, as it is the case with most symbolic-based approaches (see,
e.g.,~\cite{baldoni-symbexecsurvey-2018} for a survey), the effective
application of rewriting modulo SMT comes with new challenges in terms
of scalability: namely, the symbolic state-space explosion problem.
For instance, the symbolic semantics of
PLEXIL~\cite{rocha-thesis-2012,rocha-rewsmtjlamp-2017} ---a
synchronous language developed by NASA to support autonomous
spacecraft operations and whose formal semantics have been given
previously in rewriting logic~\cite{dowek-plexilsos-2010}--- is
complete and sound for reachability analysis. Nevertheless, the
state-space can grow very large and the constraints become complex,
making the symbolic-based formal analysis of PLEXIL plans
time-consuming or unfeasible in some cases.

This paper presents \emph{guarded terms}, a technique to reduce the
symbolic state space and the complexity of constraints in the
rewriting modulo SMT approach. A guarded term can be seen as a choice
operator that is part of the term structure in a constrained
term. Guarded terms generalize constrained terms by allowing $t$ in a
symbolic state $\pair{t}{\phi}$ to have, e.g., a guarded term
$u_1|_{\psi} \lor u_2|_{\neg\psi}$ as a subterm.  This means that when
the constraint $\phi\land\psi$ is satisfiable, $\pair{t}{\phi}$
represents those ground instances of $t$ in which the subterm
$u_1|_{\psi} \lor u_2|_{\neg\psi}$ is replaced by $u_1$.  Analogously,
when $\phi\land\neg\psi$ is satisfiable, $\pair{t}{\phi}$ represents
those ground instances of $t$ in which the subterm $u_1|_{\psi} \lor
u_2|_{\neg\psi}$ is replaced by $u_2$.
%
For example,
$\pair{f(x|_{x > 1} \vee g(x,y)|_{x \leq 1})}{y < 5}$
represents 
two constrained terms
\[
\pair{f(x)}{x > 1 \land y < 5}, 
\quad
\pair{f(g(x,y))}{x \leq 1 \land y < 5}.
\]
%
Therefore, the given guarded term can actually encode both constrained
terms as a single term. The greater potential of guarded terms can
better be seen when they are composed in parallel or nested, thus
enabling the succinct encoding of several constrained terms into one
guarded term.

Guarded terms are particularly useful in rewriting modulo SMT for
reducing: (i) the symbolic state space by implicitly encoding
branching in the term structure, and (ii) the complexity and size of
constraints by distributing them in several parts of the term
structure. The effectiveness of the approach is illustrated with a
case study of an unbounded and symbolic priority queue that, with the
help of guarded terms, enables automatic reachability analysis of the
CASH scheduling algorithm~\cite{caccamo2000capacity}.

This paper is an extended and revised version
of~\cite{bae-guarded-2017}, including:

\begin{itemize}
  \item A more comprehensive description of preliminaries on rewrite
    theories and reachability analysis in Maude.
    
  \item New results on the %simulation (i.e., soundness) and
    bisimulation (i.e., completeness) of rewriting with guarded terms
    relative to rewriting modulo SMT,
    besides the simulation  (i.e., soundness) result in \cite{bae-guarded-2017}.
    % Question: the simulation case was already explained in the FACS paper?
    
  \item Queries on the running example exhibiting the usefulness of
    guarded terms for effectively finding witnesses to %existential
    reachability goals of rewrite theories.
  \item A broader exploration of key aspects in the CASH case study
    specification in rewriting logic.
  \item Complete proofs of all results in sections ~\ref{sec.guarded}
    and~\ref{sec:guarded-rew}.
\end{itemize}

All in all, guarded terms help in making rewriting modulo SMT a more
effective and useful technique for effectively specifying, simulating,
and analyzing open system in rewriting logic.

The rest of the paper is organized as follows. Section~\ref{sec.rew}
overviews rewriting logic and Maude, while Section~\ref{sec.rewsmt}
presents the key points in the rewriting modulo SMT approach. Guarded
terms are presented in Section~\ref{sec.guarded}, while the soundness
and completeness results are stated in
Section~\ref{sec:guarded-rew}. Section~\ref{sec.cash} introduces the
case study on the CASH algorithm. Finally, Section~\ref{sec.related}
discusses related work and Section~\ref{sec.concl} presents some
concluding remarks.
