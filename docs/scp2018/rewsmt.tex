% !TEX root = main.tex

\section{Rewriting Modulo SMT in a Nutshell}
\label{sec.rewsmt}

This section briefly explains %order-sorted rewriting logic and
rewriting modulo SMT, summarizing Sections 2--5
in~\cite{rocha-rewsmtjlamp-2017}.  Rewriting modulo
SMT~\cite{rocha-rewsmtjlamp-2017} is a symbolic technique to model and
analyze reachability properties of infinite-state systems in rewriting
logic, that can be executed in Maude by querying SMT decision procedures.

\subsection{Rewriting Modulo SMT}
\label{sec:rw-smt}

Rewriting modulo SMT is illustrated with a symbolic version of
Example~\ref{ex:ch}.
In this version, 
a symbolic state is given by a
\emph{constrained term} $\pair{t}{\phi}$, with $t \in
T_\Sigma(X_\lang)_\sort{Conf}$ and $\phi \in
\oqff{\Sigma_\lang}{X_\lang}$, where $X_\lang \subseteq X$ denotes the
set of variables ranging over the built-ins  (the sorts handled by the SMT solver), 
$\Sigma_\lang \subseteq \Sigma$ denotes the signature of the built-in sorts, and
$\oqff{\Sigma_\lang}{X_\lang}$ denotes the set of quantifier-free
formulas over $\Sigma_\lang$ with variables in $X_\lang$.


\begin{example}\label{ex.srunning}
The built-in sorts are
$\sort{Bool}$ and $\sort{Int}$, and the non-built-in sorts are
$\sort{Channel}$ and $\sort{Conf}$.  There are three rewrite rules
with variables $I_1,I_2$ of sort $\sort{Int}$, variables $C_1,C_2$ of
sort $\sort{Channel}$, and $\phi_1,\phi_2$ of sort $\sort{Bool}$:
%
\begin{align*}
r_1\!\!:\;\;  &\pair{I_1\;I_2\;C_1 \parallel C_2}{\phi_1} \;\rews\;\pair{I_1 \; C_1 \parallel (I_1 + I_2 + 1)\;C_2}{\phi_1 \land \phi_2} 
  \\[-0.2em]
  &\omit\hfill\ensuremath{\textbf{if} \quad \phi_2 := 0 < I_1 \;\land\; 0< I_2 \;\land\; (I_1 + I_2 \meq{3} 0) \; \land \; \textit{sat}(\phi_1 \land \phi_2)}
\\[1ex]
r_2\!\!:\;\;  &\pair{I_1\;I_2\;C_1 \parallel C_2}{\phi_1}\;\rews \;\pair{I_1\;C_1 \parallel C_2}{\phi_1\land\phi_2} 
  \\[-0.2em]
  &\omit\hfill\ensuremath{\textbf{if} \quad \phi_2 := 0 < I_1 \;\land\; 0< I_2 \;\land\; \neg(I_1 + I_2 \meq{3} 0) \; \land \; \textit{sat}(\phi_1 \land \phi_2)}
\\[1ex]
r_3\!\!:\;\;  &\pair{I_1 \parallel I_2\;C_2}{\phi_1}\;\rews\;\pair{\textit{none} \parallel I_2 \;C_2}{\phi_1 \land \phi_2} 
  \\[-0.2em]
  &\omit\hfill\ensuremath{\textbf{if} \quad  \phi_2 := 0 < I_1 \;\land\; 0< I_2 \;\land\; (I_1 + I_2 \meq{17} 0) \; \land\; \textit{sat}(\phi_1 \land \phi_2)}
\end{align*}

These rules are similar to the ones in Example~\ref{ex:ch}.  The key
observation in this version is that conditions are treated as
constraints, accumulated in the system state, and queried for
satisfiability with the help of the function $\textit{sat}$ (an
interface to the SMT-solver to check whether $\phi$ is satisfiable or
not).  A \textit{matching condition}~\cite{clavel-maudebook-2007} of
the form $t := u$ is a syntactic variant of the equational condition
$t = u$ mathematically interpreted as an ordinary
equation. Operationally, a matching condition behaves like a `let'
construct in functional programming languages so that $t := u$
introduces $t$ in the rule as the result of reducing $u$ to canonical
form with the oriented equations modulo the axioms.
\end{example}


A \emph{built-in subtheory} $\mathcal{E}_0$ is a first-order theory
with a built-in subsignature $\Sigma_0 \subseteq \Sigma$.  The
satisfiability of a constraint $\phi \in \oqff{\Sigma_\lang}{X_\lang}$
is decided using SMT solving with respect to the corresponding
built-in algebra $\model$.  It is assumed that a given equational
theory $\mathcal{E} = (\Sigma, E)$ ``protects'' $\mathcal{E}_0$ in the
sense that the semantics of $\mathcal{E}_0$ is preserved by the
equations in $E$.  In particular, for built-in $\Sigma_0$-terms
$t_1,t_2 \in T_{\Sigma_\lang}$, if $t_1 =_E t_2$, then $\model \models
t_1 = t_2$.  A symbolic rewrite relation $\srews_\rcal$ modulo
$\mathcal{E}_0$ for constrained terms is then defined as follows.


\begin{definition}
Given a rewrite rule $\crl{\pair{l}{\phi_l}}{\pair{r}{\phi_r}}{\phi}$,
with $\phi \in \oqff{\Sigma_\lang}{X_\lang}$ and $l,r \in T_\Sigma(X)_\sort{Conf}$, a constrained term $\pair{t}{\phi_t}
$
symbolically rewrites to a constrained term 
$\pair{u}{\phi_u}
$,
denoted by $\pair{t}{\phi_t}\srews_\rcal \pair{u}{\phi_u}$, if and
only if there exists a substitution $\theta$ such that:
%
\begin{enumerate}[(a)]
	\item $\theta l =_{E} t$ and $\theta r =_{E} u$,
	\item $\model \models (\phi_l \land \theta \phi) \Leftrightarrow \phi_u$,
	and
	\item  $\phi_u$ is $\model$-satisfiable.
\end{enumerate}
\end{definition}

\noindent
The symbolic relation $\srews_\rcal$ is defined as a topmost rewrite
relation, where all rewrites take place at the top of the term,
induced by $R$ modulo $E$ on $T_\Sigma(X_\lang)$ with extra
bookkeeping of constraints.


Condition (a) can be solved by matching as in the definition of
$\rews_\rcal$ above. Condition (b) can be met by setting $\phi_u$ to
be $\phi_l \land \theta \phi$, as in the above matching conditions.
However, Condition (c) cannot be dealt with by
rewriting in general. 
%The reason is that such a condition can be an inductive
%theorem of $\tcal_{\Sigma /E \uplus B}$. 
Instead, these conditions are
checked with the help of decision procedures from an SMT
solver via the function $\textit{sat}$. Observe that, up to the choice
of the semantically equivalent $\phi_u$ for which a fixed strategy
such as the one suggested above can be assumed, the symbolic relation
$\srews_\rcal$ is deterministic in the sense of being determined by
the rule and the substitution $\theta$.%
\footnote{Here it is assumed that
variables in the rules are disjoint from the ones in the target
terms.
} 
The reader is referred to~\cite{rocha-rewsmtjlamp-2017} for
details about rewriting modulo SMT.

\begin{example}\label{ex.runningstep}
Consider the constrained term $\pair{A\;B\;C\;D \parallel
  \textit{none}}{\textit{true}}$, with four variables $A,B,C,D \in
X_{\sort{Int}}$.  %Notice that 
$\pair{A\;B\;C\;D \parallel
  \textit{none}}{\textit{true}}$ symbolically rewrites in one step to
$(A\;C\;D \parallel A + B + 1 ; 0 < A \land 0 < B \land (A + B
\meq{3} 0))$.
\end{example}

\subsection{Symbolic Reachability Analysis}

Rewriting modulo SMT can be used for solving existential reachability
goals in the initial model $\tcal_\rcal$ of a rewrite theory $\rcal$
modulo built-ins $\ecal_\lang$. In general, for any constrained term
$\pair{t}{\phi}$ where $t$ is a state term with sort $\sort{Conf}$ and
$\phi$ is a constraint, $\den{t}{\phi}$ is the \emph{denotation} of
$\pair{t}{\phi}$ consisting of all ground instances of $t$ that
satisfy $\phi$; formally 
\[
\den{t}{\phi} = \left\{t' \in
T_{\Sigma,\sort{Conf}} \mid  (\exists \func{\sigma}{X}{T_\Sigma})\;
t' = \sigma t  \;\land\; \model \models \sigma\phi
\right\}.
\]  

The type of \emph{existential reachability} question that
rewriting modulo SMT can solve can now be formulated: are there some
states in $\den{t}{\phi}$ from which is possible to reach some state
in $\den{u}{\psi}$?  Answering this question can be especially useful
for symbolically proving or disproving safety properties of $\rcal$,
such as inductive invariants %or deadlock freedom   
%\marginpar{Removed to avoid confusion}
of $\tcal_\rcal$:
when $\den{u}{\psi}$ is a set of \emph{bad} states, the idea is to
know whether reaching a state in $\den{u}{\psi}$ is possible.

\begin{example}\label{ex.squery1}
  Consider the following existential query, where $\rcal$ is the
  rewrite theory presented in Example~\ref{ex:ch} (i.e., the one
  without symbolic constraints):
%
\begin{align*}
  \tcal_\rcal \models \left(\exists \,I,C \right) \; & \,I \, (I+1) \, (I+2) \, (I+3) \parallel \textit{none} \;\rews_\rcal^*\;\textit{none} \parallel C \;\land\; I > 0.
\end{align*}

This query asks if it is possible to find an initial state consisting
of four consecutive positive integers in the left channel and no
number on the right channel that leads to a state where all numbers
in the left channel have been processed.
%
This is the same type of reachability property discussed before,
namely, the one in which the interest is in finding initial states
that can lead to terminal states that are not deadlocks.

Answering this query in the positive indicates that $\rcal$ can be
executed from some states satisfying the initial pattern and 
reach a desired terminal  state. Since $I$ ranges over an infinite
domain, this question cannot be solved directly via rewriting and
would require, e.g., inductive reasoning over $\rews_\rcal$.  However,
the following Maude \textit{search} command can be issued in Maude to
find a proof (or a counterexample) for the symbolic rewrite relation
$\srews_\rcal$:
%
\begin{maude}
search [1]  { I I+1 I+2 I+3 || none, I > 0 } 
       =>*  { none | C, Phi } .
\end{maude}
%
Executed as it is, this command times out after 5 minutes.
\end{example}

%NOTE: Moved to Section 5.4
%
%\begin{example}\label{ex.squery2}
%  Consider the following existential query, where $\rcal$ is the
%  rewrite theory used in Example~\ref{ex.squery1} (i.e., the rewrite
%  theory introduced in Example~\ref{ex:ch}):
%%
%\begin{align*}
%  \tcal_\rcal \models \left(\exists \,I,C_1,C_2 \right) \; & \,I \, (I+1) \, (I+3) \, (I+4) \parallel \textit{none} \;\rews_\rcal^*\;C_1 \parallel C_2 \,\land\, I> 1 
%  \\ & \land \, C_1 \neq \textit{none} \, \land \,  \textit{``$C_1 \parallel C_2$ is $\rews_\rcal$-irreducible''}.
%\end{align*}
%
%This query asks if it is possible to find an initial state consisting
%of four consecutive positive integers in the left channel and no
%number on the right channel that leads to a terminal (i.e.,
%irreducible) state where there are still numbers in the left channel.
%This is the same type of deadlock freedom property discussed before,
%namely, the one in which bad states are those irreducible ones in
%which the channel has at least one number.
%
%Answering this query in the negative proves that $\rcal$ is deadlock
%free for any initial state satisfying the initial pattern. As in
%Example~\ref{ex.squery2}, the variable $I$ ranges over an infinite
%domain, and this question cannot be solved directly via rewriting.
%The following Maude \textit{search} command can be issued in Maude to
%find a proof (or a counterexample) for the symbolic rewrite relation
%$\srews_\rcal$:
%%
%\begin{maude}
%search [1] { I I+1 I+2 I+3 || none , I > 1 }
%       =>! { C1 || C2, Phi } such that C1 =/= none .
%\end{maude}
%%
%This search command also times out after 5 minutes.
%\end{example}
%
%As shown in Section~\ref{sec:guarded-rew}, with the help of guarded
%terms, the equivalent search commands introduced in
%examples~\ref{ex.squery1} and~\ref{ex.squery2} terminate in less than
%1 second.

%% therefore proving the deadlock freedom of $\tcal_\rcal$ from states
%% satisfying $I \; (I+1) \; (I+2) \; (I+3) \parallel \textit{none}$.
