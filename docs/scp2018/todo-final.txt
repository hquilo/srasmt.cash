- Reviewer 2

  - I think the authors have successfully addressed all comments of
the reviewers and the revised version has been improved substantially
by correcting all mistakes and inconsistencies.

The contribution seems an important step ahead in symbolic
specification and verification of infinite state systems with
experimental evidence about its applicability.

I fully support acceptance of the paper.

Some minor errors/typos are listed below:

[DONE] P8,Ex.3.1,rule r3: why \phi_1 is not considered in the rhs?

[DONE] P8,Def.3.1,#285: remove ) before if and only if

[TODO] P12,Ex.4.1,L5: you say there are 8 different cases to consider,
but not how many steps are considered

[DONE] P12,#382: given by -> reachable from (?)

[DONE] P16,#493: u_1...u_n -> v_1 ... v_n

[DONE] P16,#500: add a note about the fact that standard form is not
unique by saying e.g. that 0|true and 0|true \/ 0|false are standard
forms for 0.

[DONE] P18,#550: becuase -> because

[DONE] P20,#614: add that "u is admissible for a set of rules if it is
admissible for all rules" (?)

[TODO] P28,#896: can you elaborate on the definition of (e const:
\phi) and explain why this notation is needed/useful?

[TODO] P29: maybe for one rule it could be informative to show the
previous rules from [2] that it subsumes.  For example you can show
the rules summarized by stopExecuting1

[TODO] P31,#1008: why I0 and not I1

[DONE] P34,#1111: completeness -> complete

